package com.company;

import com.company.colors.Blue;
import com.company.colors.Color;
import com.company.colors.Green;
import com.company.colors.Red;
import com.company.objects.Shape;

/**
 * Created by Denis on 06.09.2015.
 */
public class ColorFactory extends AbstractFactory {

    @Override
    public Shape getShape(String shapeType){
        return null;
    }

    @Override
    Color getColor(String color) {

        if(color == null){
            return null;
        }

        if(color.equalsIgnoreCase("RED")){
            return new Red();

        }else if(color.equalsIgnoreCase("GREEN")){
            return new Green();

        }else if(color.equalsIgnoreCase("BLUE")){
            return new Blue();
        }

        return null;
    }
}
