package com.company;

import com.company.colors.Color;
import com.company.objects.Shape;

/**
 * Created by Denis on 06.09.2015.
 */
public abstract class AbstractFactory {
    abstract Color getColor(String color);
    abstract Shape getShape(String shape) ;
}
