package com.company;

/**
 * Created by Denis on 06.09.2015.
 */
public interface Packing {
    public String pack();
}
