package com.company;

/**
 * Created by Denis on 06.09.2015.
 */
public interface Item {
    public String name();
    public Packing packing();
    public float price();
}
